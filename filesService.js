const fs = require('fs');
const path = require('path');

const FILE_PATH = './files/';

function createFile (req, res, next) {
  let filesNames = fs.readdirSync(FILE_PATH);

  if (filesNames.includes(req.body.filename)) {
    next({status: 400, 'message': `File with name ${req.body.filename} already exists`});
  }

  if (!req.body.filename) {
    next({status: 400, 'message': 'Please specify filename parameter'});
  }

  if (!req.body.content) {
    next({status: 400, 'message': 'Please specify content parameter'});
  }

  const fileExtension = req.body.filename.split('.')[req.body.filename.split('.').length - 1]

  if (!['log', 'txt', 'json', 'yaml', 'xml', 'js'].includes(fileExtension)) {
    next({status: 400, 'message': 'File extension should be log, txt, json, yaml, xml or js'});
  }

  fs.writeFileSync(`${FILE_PATH}${req.body.filename}`, req.body.content);

  if (req.body.password) {
    let passwords = {};

    if (fs.existsSync('passwords.json')) {
      passwords = JSON.parse(fs.readFileSync('passwords.json', {encoding: 'utf8', flag: 'r'}));
    }

    passwords = JSON.stringify({...passwords, [req.body.filename]: req.body.password});

    fs.writeFileSync('passwords.json', passwords);
  }

  res.status(200).send({ "message": "File created successfully" });
}

function getFiles (req, res, next) {
  let filesNames = fs.readdirSync(FILE_PATH);

  res.status(200).send({
    "message": "Success",
    "files": filesNames
  });
}

const getFile = (req, res, next) => {
  const filesNames = fs.readdirSync(FILE_PATH);

  const fileName = req.params.filename;

  if (fs.existsSync('passwords.json')) {
    const passwords = JSON.parse(fs.readFileSync('passwords.json', {encoding: 'utf8', flag: 'r'}));

    if (fs.existsSync('passwords.json') && passwords.hasOwnProperty(fileName)) {
      if (!req.query.hasOwnProperty('password')) {
        next({status: 400, 'message': `File ${fileName} is password protected`});
      }

      const password = req.query.password;

      if (passwords[fileName] !== password) {
        next({status: 400, 'message': `Password for file ${fileName} is invalid`});
      }
    }
  }

  if (!filesNames.includes(fileName)) {
    next({status: 400, 'message': `No file with ${fileName} filename found`});
  }

  const file_fd = fs.openSync(`${FILE_PATH}${fileName}`);
  let fileStat = fs.fstatSync(file_fd);

  const content = fs.readFileSync(`${FILE_PATH}${fileName}`, {encoding:'utf8', flag:'r'});

  const extension = path.extname(req.params.filename).split('.').join("");

  res.status(200).send({
    "message": "Success",
    "filename": fileName,
    "content": content,
    "extension": extension,
    "uploadedDate": fileStat.birthtime
  });
}

function editFile (req, res, next) {
  const filesNames = fs.readdirSync(FILE_PATH);

  const fileName = req.params.filename;

  if (fs.existsSync('passwords.json')) {
    const passwords = JSON.parse(fs.readFileSync('passwords.json', {encoding: 'utf8', flag: 'r'}));

    if (passwords.hasOwnProperty(fileName)) {
      if (!req.query.hasOwnProperty('password')) {
        next({status: 400, 'message': `File ${fileName} is password protected`});
      }

      const password = req.query.password;

      if (passwords[fileName] !== password) {
        next({status: 400, 'message': `Password for file ${fileName} is invalid`});
      }
    }
  }

  if (!filesNames.includes(fileName)) {
    res.status(400).send({'message': 'File doesn\'t exist'});
    return;
  }

  if (!req.body.content) {
    next({status: 400, 'message': 'Please specify content parameter'});
  }

  fs.writeFileSync(`${FILE_PATH}${fileName}`, req.body.content, { flag: 'a' });

  res.status(200).send({ "message": "File updated successfully" });
}

function deleteFile (req, res, next) {
  const filesNames = fs.readdirSync(FILE_PATH);

  const fileName = req.params.filename;

  if (!filesNames.includes(fileName)) {
    next({status: 400, 'message': 'File doesn\'t exist'});
  }

  if (fs.existsSync('passwords.json')) {
    let passwords = JSON.parse(fs.readFileSync('passwords.json', {encoding: 'utf8', flag: 'r'}));

    if (passwords.hasOwnProperty(fileName)) {
      if (!req.query.hasOwnProperty('password')) {
        next({status: 400, 'message': `File ${fileName} is password protected`});
      }

      const password = req.query.password;

      if (passwords[fileName] !== password) {
        next({status: 400, 'message': `Password for file ${fileName} is invalid`});
      }
    }

    delete passwords[fileName]

    fs.writeFileSync('passwords.json', JSON.stringify(passwords));
  }

  fs.rmSync(`${FILE_PATH}${fileName}`);

  res.status(200).send({ "message": "File deleted successfully" });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
